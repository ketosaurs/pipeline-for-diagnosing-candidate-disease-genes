
#### figure out how to create a uniqe list form a identety matrix

install.packages("GGally")
library(GGally)
library(network)
library(sna)
library(ggplot2)


distnet = network(Distonia, directed = F, names.eval, "wie")
ggnet2(distnet, node.size = 6, node.color = "red")



net = rgraph(10, mode = "graph", tprob =0.5)
net= network(net, directed = F)
network.vertex.names(net) = letters[1:10]
ggnet2(net)


ggnet2(net, size = 6, color = rep(c("tomato", "steelblue"),5), edge.size = 1, edge.color = "grey")

net %v% "phono" = ifelse(letters[1:10] %in% c("a", "e", "i"), "vowel", "consonant")
ggnet2(net, color = "phono", palette = "Set2")


ggnet2(net, alpha = "phono", alpha.legend = "legend", label = T)


bip = network(Distonia, matrix.type = "adjacency", names.eval = "weights")
ggnet2(bip, edge.label = "weigths")


diptest = data.frame(event1 = c(1, 2, 1, 0),
                 event2 = c(0, 0, 3, 0),
                 event3 = c(1, 1, 0, 4),
                 row.names = letters[1:4])
diptest
Distonia
bip = network(diptest,
              matrix.type = "bipartite",
              ignore.eval = FALSE,
              names.eval = "weights")
ggnet2(bip, label = T, edge.label = "weights" )

distbip = network(Distonia, matrix.type = "bipartite", ignore.eval = F, names.eval="weights")
ggnet2(distbip, label = T)


diptest
rownames(diptest[1,])
colnames(diptest[1])


diptest2 = data.frame(a = c(0, 2, 1),
                      b = c(2, 0, 3),
                      c = c(1, 3, 0 ),
                      row.names = letters[1:3])
diptest2


networkframe <- data.frame(Source = character(),
                           Target = character(),
                           zscore = double(),
                           stringsAsFactors = FALSE)

nrow(networkframe)

for (i in c(1 :nrow(diptest2))) {
  source <- rownames(diptest2[i,])
  for(j in c(1: ncol(diptest2))){
    target <- colnames(diptest2[j])
    if (source == target) {
      cat("source and target are the same\n")
      show(source)
      show(target)
      next
    }else {if (j < i){
      zscore <- diptest2[j,i]
    }
      zscore <- diptest2[i,j]
    }
    if (target %in% networkframe$Source & source %in% networkframe$Target) {
      cat("source target combination had been made before\n")
      next
    }else {
    networkframe <- rbind(networkframe, data.frame(Source = source, Target= target, Zscore = zscore))
    }
  }
}


networkframe


#### create a anotation table

anotationtable <- data.frame(Gene_id = character(),
                             Cluster = integer(),
                             Phenotype = character(),
                             stringsAsFactors = FALSE)

for (i in 1: max(cluster_object$cluster)) {
  GenesInCluster = cluster_object$labels[cluster_object$cluster == i]
  for (gene in GenesInCluster){
    CatalogeEntery<- GenePanelCataloge[GenePanelCataloge$gene_id == gene,]
    if ("Distonia" %in% CatalogeEntery$panel_1 & "Myoclonus" %in% CatalogeEntery$panel_2 ) {
      pheno <- "MyoclonusDistonia"
    } else {if ("Distonia" %in% CatalogeEntery$panel_1) {
      pheno <- "Distonia"
    } else {if ("Myoclonus" %in% CatalogeEntery$panel_2){
      pheno <- "Myoclonus"
    } else {pheno <- "unpanneld"}}}
  anotationtable <- rbind(anotationtable, data.frame(Gene_id = gene, Cluster=i, Phenotype = pheno))
}}
anotationtable


