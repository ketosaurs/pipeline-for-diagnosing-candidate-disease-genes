# load the nessesary data
networkFromFile <- read.csv(paste0("Pipeline",OSsep,"Outputs",OSsep,Analysis_name,OSsep,Analysis_name,"_networkframe.csv"), row.names = 1)
annotation <- read.csv(paste0("Pipeline",OSsep,"Outputs",OSsep,Analysis_name,OSsep,Analysis_name,"_anotation.csv"), row.names = 1)

library(data.table, quietly=T)
library(visNetwork, quietly=T)
library(htmlwidgets, quietly=T)

# filter the edges of the network. so only the edges that are significant will be shown
filterd_network <- subset(networkFromFile, networkFromFile$Zscore >= 2.7)

# make sure that all the genes will be shown in the plot. for this the annotation file is used,
# since this has all the genes and also knows to what cluster a gene belongs
nodes <- data.frame(id = annotation$Gene_id, group = annotation$Cluster, label = annotation$Gene_name)

# edges show the connection between genes and can show the strength of this relation
edges <- data.frame(from=filterd_network$Source, to=filterd_network$Target, value=filterd_network$Zscore)

# create the network with a legend. one can hover over a node to see the gene name.
Network <-visNetwork(nodes, edges, main = paste0("gene network"),
           height = 800, width = 1800, background = "lightgray")%>%
visLegend(main = "cluster number", zoom=F,  ) %>%
visInteraction(dragNodes = F)%>%
visOptions(highlightNearest = list(enabled =TRUE, degree = 1, hover = T), selectedBy = "group")%>%
visIgraphLayout()

# save the network
setwd(paste0("Pipeline",OSsep,"Outputs",OSsep,Analysis_name))
htmlwidgets::saveWidget(Network,paste0(Analysis_name,"_network.html"))
setwd(paste0("..",OSsep,"..",OSsep,"..",OSsep))
